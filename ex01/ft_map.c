/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_map.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rblondia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/16 20:07:03 by rblondia          #+#    #+#             */
/*   Updated: 2021/09/16 20:08:33 by rblondia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int	*ft_map(int *tab, int length, int (*f)(int))
{
	int	i;
	int	*t;

	t = malloc(sizeof(int) * length);
	i = 0;
	while (i < length)
	{
		t[i] = f(tab[i]);
		i++;
	}
	return (t);
}
