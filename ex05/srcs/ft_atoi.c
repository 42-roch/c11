/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rblondia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/08 12:06:16 by rblondia          #+#    #+#             */
/*   Updated: 2021/09/21 14:42:50 by rblondia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	isnumber(char a)
{
	if (a >= '0' && a <= '9')
		return (0);
	return (1);
}

int	ispositive(char *str)
{
	int	n;
	int	a;

	n = 0;
	a = 0;
	while (str[a])
	{
		if (n > 0 && str[a] != '+' && str[a] != '-')
			break ;
		if (str[a] == '-')
			n++;
		a++;
	}
	if (n % 2 == 0)
		return (0);
	return (1);
}

int	ft_atoi(char *str)
{
	int	i;
	int	target;

	target = 0;
	i = 0;
	while (str[i])
	{
		if (isnumber(str[i]) == 1 && !(str[i] == '-' || str[i] == '+'))
			break ;
		else if (str[i] == '\0' || (str[i] >= 9 && str[i] <= 13)
			|| str[i] == 32 || isnumber(str[i]) == 1)
		{
			i++;
			continue ;
		}
		target = target * 10 + (str[i] - '0');
		i++;
	}
	if (ispositive(str) == 1)
		target = -target;
	return (target);
}
