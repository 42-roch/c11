/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   do-op.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rblondia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/16 21:31:10 by rblondia          #+#    #+#             */
/*   Updated: 2021/09/22 17:02:07 by rblondia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/proto.h"
#include "../includes/messages.h"

int	process_operator(int a, int b, char operator, int *result)
{
	if (operator == '%')
	{
		if (b == 0)
		{
			ft_putstr(ZERO_MODULO);
			return (-1);
		}
		*result = a % b;
	}
	else if (operator == '/')
	{
		if (b == 0)
		{
			ft_putstr(ZERO_DIVISION);
			return (-1);
		}
		*result = a / b;
	}
	else if (operator == '*')
		*result = a * b;
	else if (operator == '-')
		*result = a - b;
	else if (operator == '+')
		*result = a + b;
	return (0);
}

int	main(int argc, char **argv)
{
	int	a;
	int	b;
	int	result;
	int	error;

	if (argc != 4)
		return (0);
	result = 0;
	a = ft_atoi(argv[1]);
	b = ft_atoi(argv[3]);
	error = process_operator(a, b, argv[2][0], &result);
	if (error == -1)
		return (0);
	ft_putnbr(result);
	ft_putstr("\n");
	return (0);
}
