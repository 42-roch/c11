/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is_sort.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rblondia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/16 20:15:30 by rblondia          #+#    #+#             */
/*   Updated: 2021/09/21 15:17:25 by rblondia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_is_sorted(int *tab, int length, int (*f)(int, int))
{
	int	i;

	i = 0;
	while (i < length - 1)
	{
		if (f(tab[i], tab[i + 1]) > 0)
			return (0);
		++i;
	}
	return (1);
}

int	ft_is_sorted_(int *tab, int length, int (*f)(int, int))
{
	int	i;

	i = 0;
	while (i < length - 1)
	{
		if (f(tab[i], tab[i + 1]) < 0)
			return (0);
		++i;
	}
	return (1);
}

int	ft_is_sort(int *tab, int length, int (*f)(int, int))
{
	if (ft_is_sorted(tab, length, f)
		|| ft_is_sorted_(tab, length, f))
		return (1);
	return (0);
}
